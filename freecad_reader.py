import sys
sys.path.append("/home/adirbens/freecad-build/lib/")

import FreeCAD


class FreecadReader:


    def TryLoadObject(self, path):
        try:
            self.doc = FreeCAD.open(path)
            self.object = self.doc.Objects[0]
            self.shape = self.object.Shape
            self.bbox = self.shape.BoundBox
            self.center = self.shape.CenterOfMass
            self.diagonalLength = self.bbox.DiagonalLength
            return True
        except Exception as e:
            print(e)
            return False


    def GetMinX(self):
        return round(self.bbox.XMin, 3)

    def GetMinY(self):
        return round(self.bbox.YMin, 3)

    def GetMinZ(self):
        return round(self.bbox.ZMin, 3)

    def GetMaxX(self):
        return round(self.bbox.XMax, 3)

    def GetMaxY(self):
        return round(self.bbox.YMax, 3)

    def GetMaxZ(self):
        return round(self.bbox.ZMax, 3)
    

    def GetCenterX(self):
        return round(self.center[0], 3)
    
    def GetCenterY(self):
        return round(self.center[1], 3)
    
    def GetCenterZ(self):
        return round(self.center[2], 3)
    
    
    def GetRadius(self):
        return round(max(self.bbox.XLength, self.bbox.YLength, self.bbox.ZLength) / 2.0, 3)


    def IsPointInside(self, x, y, z, tolerance, checkFace):
        point = FreeCAD.Vector(x, y, z)
        return self.shape.isInside(point, tolerance, checkFace)
