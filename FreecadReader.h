#include <Python.h>

#define X_DIM 0
#define Y_DIM 1
#define Z_DIM 2

#define FAILURE_CODE 1



class FreecadReader
{
    private:

        PyObject* module = NULL;
        PyObject* object = NULL;

    public:

        void Initialize(const char* pythonScriptDirPath, const char* pythonScriptName);
        void Finalize();

        bool LoadObject(const char* freecadFilePath);

        float GetMin(int dim);
        float GetMax(int dim);
        float GetCenter(int dim);

        float GetRadius(); // NO TRUST

        bool IsPointInside(float x, float y, float tolerance=0.0001, bool checkSurface=true);
        bool IsPointInside(float x, float y, float z, float tolerance=0.0001, bool checkSurface=true);

};