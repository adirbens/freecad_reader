#include <stdlib.h>
#include <freecadReader.h>
#include <iostream>

using namespace std;


void FreecadReader::Initialize(const char * pythonScriptDirPath, const char* pythonScriptName)
{
    const char* name = "PYTHONPATH";
    const char* env_p = getenv(name);
    setenv("PYTHONPATH", pythonScriptDirPath, 1);

    Py_Initialize();

    PyObject* moduleName = PyUnicode_DecodeFSDefault(pythonScriptName);
    if (moduleName == NULL)
    {
        PyErr_Print();
        fprintf(stderr, "Failed to decode string\n");
        exit(FAILURE_CODE);
    }
    module = PyImport_Import(moduleName);
    Py_DECREF(moduleName);

    if (module == NULL)
    {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", pythonScriptName);
        exit(FAILURE_CODE);
    }
    
    PyObject* classObj = PyObject_GetAttrString(module, "FreecadReader");
    if (classObj == NULL)
    {
        PyErr_Print();
        fprintf(stderr, "Failed to load FreecadReader class\n");
        exit(FAILURE_CODE);
    }

    object = PyObject_CallObject(classObj, NULL);
    Py_DECREF(classObj);
    if (object == NULL)
    {
        PyErr_Print();
        fprintf(stderr, "Failed to create FreecadReader instance\n");
        exit(FAILURE_CODE);
    }
}

void FreecadReader::Finalize()
{
    if (module != NULL)
        Py_DECREF(module);
    if (object != NULL)
        Py_DECREF(object);

    Py_Finalize();
}

bool FreecadReader::LoadObject(const char * freecadFilePath)
{
    if (module == NULL)
    {
        fprintf(stderr, "Shouldn't load freecad object before module is loaded\n");
        exit(FAILURE_CODE);
    }

    if(object == NULL)
    {
        fprintf(stderr, "Can't load freecad object when object is null\n");
        exit(FAILURE_CODE);
    }
    
    PyObject* filePath = PyUnicode_DecodeFSDefault(freecadFilePath);
    PyObject* methodName = PyUnicode_FromString("TryLoadObject");

    if (methodName != NULL and filePath != NULL)
    {
        PyObject* result = PyObject_CallMethodObjArgs(object, methodName, filePath, NULL);

        if (result != NULL)
        {
            bool res = PyObject_IsTrue(result);
            if (!res)
            {
                Py_DECREF(result);
                Py_DECREF(filePath);
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "LoadObject function returned false\n");
                exit(FAILURE_CODE);
            }
        }
        else
        {
            Py_DECREF(filePath);
            Py_DECREF(methodName);

            PyErr_Print();
            fprintf(stderr, "LoadObject function failed\n");
            exit(FAILURE_CODE);
        }
    }
    else
    {
        PyErr_Print();
        fprintf(stderr, "Failed to decode strings\n");
        exit(FAILURE_CODE);
    }

    Py_DECREF(filePath);
    Py_DECREF(methodName);

    return true;
}

float FreecadReader::GetMin(int dim)
{
    if (dim < 0 or dim >= 3)
    {
        fprintf(stderr, "Invalid dim value\n");
            exit(FAILURE_CODE);
    }

    const char* name;
    if (dim == X_DIM)
        name = "GetMinX";
    else if (dim == Y_DIM)
        name = "GetMinY";
    else
        name = "GetMinZ";

    if (object != NULL)
    {
        PyObject* methodName = PyUnicode_DecodeFSDefault(name);

        if (methodName != NULL)
        {
            PyObject* result = PyObject_CallMethodObjArgs(object, methodName, NULL);

            if (result != NULL)
            {
                float res = PyFloat_AsDouble(result);

                Py_DECREF(methodName);
                Py_DECREF(result);

                return res;
            }
            else
            {
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "%s function failed\n", name);
                exit(FAILURE_CODE);
            }
            
        }
        else
        {
            PyErr_Print();
            fprintf(stderr, "Failed to decode string\n");
            exit(FAILURE_CODE);
        }
    }
    return -999.999;
}

float FreecadReader::GetMax(int dim)
{
    if (dim < 0 or dim >= 3)
    {
        fprintf(stderr, "Invalid dim value\n");
            exit(FAILURE_CODE);
    }

    const char* name;
    if (dim == X_DIM)
        name = "GetMaxX";
    else if (dim == Y_DIM)
        name = "GetMaxY";
    else
        name = "GetMaxZ";

    if (object != NULL)
    {
        PyObject* methodName = PyUnicode_DecodeFSDefault(name);

        if (methodName != NULL)
        {
            PyObject* result = PyObject_CallMethodObjArgs(object, methodName, NULL);

            if (result != NULL)
            {
                float res = PyFloat_AsDouble(result);

                Py_DECREF(methodName);
                Py_DECREF(result);

                return res;
            }
            else
            {
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "%s function failed\n", name);
                exit(FAILURE_CODE);
            }
            
        }
        else
        {
            PyErr_Print();
            fprintf(stderr, "Failed to decode string\n");
            exit(FAILURE_CODE);
        }
    }
    return -999.999;
}

float FreecadReader::GetCenter(int dim)
{
    if (dim < 0 or dim >= 3)
    {
        fprintf(stderr, "Invalid dim value\n");
            exit(FAILURE_CODE);
    }

    const char* name;
    if (dim == X_DIM)
        name = "GetCenterX";
    else if (dim == Y_DIM)
        name = "GetCenterY";
    else
        name = "GetCenterZ";

    if (object != NULL)
    {
        PyObject* methodName = PyUnicode_DecodeFSDefault(name);

        if (methodName != NULL)
        {
            PyObject* result = PyObject_CallMethodObjArgs(object, methodName, NULL);

            if (result != NULL)
            {
                float res = PyFloat_AsDouble(result);

                Py_DECREF(methodName);
                Py_DECREF(result);

                return res;
            }
            else
            {
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "%s function failed\n", name);
                exit(FAILURE_CODE);
            }
            
        }
        else
        {
            PyErr_Print();
            fprintf(stderr, "Failed to decode string\n");
            exit(FAILURE_CODE);
        }
    }
    return -999.999;
}

float FreecadReader::GetRadius()
{
    if (object != NULL)
    {
        PyObject* methodName = PyUnicode_DecodeFSDefault("GetRadius");

        if (methodName != NULL)
        {
            PyObject* result = PyObject_CallMethodObjArgs(object, methodName, NULL);

            if (result != NULL)
            {
                float res = PyFloat_AsDouble(result);

                Py_DECREF(methodName);
                Py_DECREF(result);

                return res;
            }
            else
            {
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "GetRadius function failed\n");
                exit(FAILURE_CODE);
            }
            
        }
        else
        {
            PyErr_Print();
            fprintf(stderr, "Failed to decode string\n");
            exit(FAILURE_CODE);
        }
    }
    return -999.999;
}

bool FreecadReader::IsPointInside(float x, float y, float tolerance, bool checkSurface)
{
    float z = GetCenter(Z_DIM);
    return IsPointInside(x, y, z, tolerance, checkSurface);
}

bool FreecadReader::IsPointInside(float x, float y, float z, float tolerance, bool checkSurface)
{
    if (object != NULL)
    {
        PyObject* methodName = PyUnicode_DecodeFSDefault("IsPointInside");

        if (methodName != NULL)
        {
            
            PyObject* xPy = PyFloat_FromDouble(x);
            PyObject* yPy = PyFloat_FromDouble(y);
            PyObject* zPy = PyFloat_FromDouble(z);
            PyObject* tolerancePy = PyFloat_FromDouble(tolerance);
            PyObject* checkSurfacePy = PyBool_FromLong(checkSurface);

            if (xPy == NULL || yPy == NULL || zPy == NULL || tolerancePy == NULL || checkSurfacePy == NULL)
            {
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "Failed to decode arguments for function IsPointInside\n");
                exit(FAILURE_CODE);
            }

            PyObject* result = PyObject_CallMethodObjArgs(object,
                                    methodName, xPy, yPy, zPy, tolerancePy, checkSurfacePy, NULL);

            if (result != NULL)
            {
                float res = PyFloat_AsDouble(result);

                Py_DECREF(xPy);
                Py_DECREF(yPy);
                Py_DECREF(zPy);
                Py_DECREF(tolerancePy);
                Py_DECREF(checkSurfacePy);
                Py_DECREF(methodName);
                Py_DECREF(result);

                return res;
            }
            else
            {
                Py_DECREF(xPy);
                Py_DECREF(yPy);
                Py_DECREF(zPy);
                Py_DECREF(tolerancePy);
                Py_DECREF(checkSurfacePy);
                Py_DECREF(methodName);

                PyErr_Print();
                fprintf(stderr, "IsPointInside function failed\n");
                exit(FAILURE_CODE);
            }
            
        }
        else
        {
            PyErr_Print();
            fprintf(stderr, "Failed to decode string\n");
            exit(FAILURE_CODE);
        }
    }
    return false;
}
